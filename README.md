# Zendesk Challenge

* [Challenge Details](docs/challenge.md)
* [Help Center](docs/help_center.md)
* [Sinatra Notes](docs/sinatra.md)
* [NASA Astronomy Picture of the Day (APOD)](docs/nasa_api.md)

# Running App Locally

You ran run the application locally and access it at [http://localhost:9292](http://localhost:9292) by using the `rackup` command.

	$ bundle exec rackup
