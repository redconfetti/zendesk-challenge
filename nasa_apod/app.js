(function() {

  return {
    events: {
      'app.activated':'getTodaysApod',
      'change .apodDate': function(event) {
        this.switchTo('loading');
        var newDate = event.currentTarget.value;
        this.ajax('fetchApod', newDate).done(function(data) {
          var viewData = {
            selectedDate: newDate,
            apod: data
          };
          this.switchTo('apod', viewData);
          this.$('.apod .alert').hide();
        }).fail(function(data){
          var errorData = {};
          if (data && data.responseJSON && data.responseJSON.msg) {
            errorData = {
              error: data.responseJSON.msg,
              selectedDate: newDate
            };
          } else {
            errorData = {
              error: 'You must choose a date between Jun 16, 1995 and today.',
              selectedDate: newDate
            };
          }
          this.switchTo('error', errorData);
        });
      },
    },

    requests: {
      fetchApod: function(date) {
        return {
          url:      'https://api.nasa.gov/planetary/apod?api_key={{setting.nasaApiKey}}&date=' + date,
          type:     'GET',
          dataType: 'json',
          secure:    true
        };
      }
    },

    getTodaysApod: function() {
      this.switchTo('loading');
      var currentdate = new Date();
      var todaysDate = currentdate.getFullYear() + '-' + ('0' + (currentdate.getMonth()+1)).slice(-2) + '-' + ('0' + (currentdate.getDate())).slice(-2);
      this.ajax('fetchApod', todaysDate).done(function(data) {
        var viewData = {
          selectedDate: todaysDate,
          apod: data
        };
        this.switchTo('apod', viewData);
      });
    }
  };

}());
