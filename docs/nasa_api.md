## NASA API

* [NASA Open APIs](https://api.nasa.gov/index.html)
* [NASA Astronomy Picture of the Day (APOD)](http://apod.nasa.gov/apod/astropix.html)
* [NASA APOD API](https://api.nasa.gov/api.html#apod)

### Help Center Application

This seems to be the least straight forward part of the challenge. This needs to be an app that is available not only to the support agents, but also for Help Center users (i.e. hosting clients). 

Hopefully this can be developed like an HTML/CSS/Javascript widget that is simply placed somewhere in the app, and I can simply use the Zendesk App API to place the same widget in both places (agent area and help center).

Example Request:
[https://api.nasa.gov/planetary/apod?api_key=8tKXFJvk4bzxmNizdRyj62p8ouqTEIo4LCoJO7FP&date=2016-01-12](https://api.nasa.gov/planetary/apod?api_key=8tKXFJvk4bzxmNizdRyj62p8ouqTEIo4LCoJO7FP&date=2016-01-12)

* [Zendesk Demo Apps](https://github.com/zendesk/demo_apps)
