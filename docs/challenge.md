# Zendesk Challenge Details

> We have decided to give you the take home test. 
> Create a trial of Zendesk and do the following: 
> 
> 1. Skin the Help Center. You can use the [ZenDesk Pinterest - Beautiful Help Centers](http://www.pinterest.com/zendesk/custom-branded-zendesks/) page for inspiration.
> 2. Install conditional fields for the help center and the Agent interface. The End User conditional fields code for help center can be found in 
[Conditional fields for the Help Center](https://github.com/skipjac/Zendesk-Apps/tree/master/conditional-fields-help-center)
> 
> The Agent conditional fields app code can be found in [Conditional Fields App](https://github.com/skipjac/Zendesk-Apps/tree/master/ConditionalFieldsApp) 
> 
> For the agent interface you will need to read up on creating apps which can be found in the [Introduction to ZenDesk Apps](http://developer.zendesk.com/documentation/apps/)
> 
> I would recommend installing our app development tool called [ZAT (Zendesk app tools)](http://developer.zendesk.com/documentation/apps/reference/tools.html)
> 
> I would like you to build an agent app that displays the NASA image of the day. When the app 1st loads it will display todays image, but you should be able to select from a calendar the image for that day. In the app the image will be thumbnail size, but when you click on the image it should open a modal with the larger image.  The API is pretty simple you can read about it in the [NASA API - Astronomy Picture of the Day Docs](https://api.nasa.gov/api.html#apod). Your NASA API key is `api_key=8tKXFJvk4bzxmNizdRyj62p8ouqTEIo4LCoJO7FP`.
> 
> And also do it for HelpCenter for a logged in user. We do use a proxy for HelpCenter to make cross domain calls here is [an example](https://github.com/skipjac/zendesk-widgets/blob/97a52a554e45468535576236540131b2088dcfdb/hipchatRoom.js).
> 
> As always feel free to Ask Me Anything, remember this is all about how well you can work in a collaborative environment.
> 
> So you can't ask too many questions. You have until 19th to complete
> 
> Cheers
> Skip
