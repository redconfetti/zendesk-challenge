## Sinatra

This application uses Sinatra, a lightweight Ruby framework. 

### Resources

* [Github - Sinatra](https://github.com/sinatra/sinatra)
* [Bundler with Sinatra](http://bundler.io/sinatra.html)
* [Sinatra Intro - Using a Classic Style Application with a config.ru](http://www.sinatrarb.com/intro.html#Using%20a%20Classic%20Style%20Application%20with%20a%20config.ru)
* [Sinatra API Example](https://github.com/sklise/sinatra-api-example)
* [Templates with yield and nested layouts](http://www.sinatrarb.com/intro.html#Templates%20with%20%3Ccode%3Eyield%3C/code%3E%20and%20nested%20layouts)
