## Help Center Notes

See Help Center demonstration at [https://redconfetti.zendesk.com/](https://redconfetti.zendesk.com/)

This demonstration seeks to provide a "Support" area for a fictional "Redconfetti Hosting" service.

* [ZenDesk Support - Getting Started with Help Center](https://support.zendesk.com/hc/en-us/articles/203664346-Getting-started-with-Help-Center)
* [Help Center Resources](https://support.zendesk.com/hc/en-us/articles/204231676-Help-Center-resources)
* [Customizing Help Center](https://support.zendesk.com/hc/en-us/articles/203664326-Customizing-Help-Center#topic_r5r_xlw_n3)
* [Help Center Templates](https://developer.zendesk.com/apps/docs/help-center-templates/introduction)

### Analysis

The ZenDesk help center is primarily made up of articles, organized into categories and/or sections. Looking over the [help center examples](http://www.pinterest.com/zendesk/custom-branded-zendesks/), it appears common for the customized help centers to provide a "Getting Started" article, or provide a "Frequently Asked Questions (FAQ)" category of articles.

I've seen some examples also provide a feed of some sort labelled "News", perhaps based on a blog RSS feed. Another one provided a few recent Tweets from their Twitter feed.

Common to any custom branded Help Center are links to external content on the clients own hosted website. For example the League of Legends Help Center provided links to the following resources, which one of their own customers might be seeking:

* Account
* Billing & Store
* Gameplay and Features

### Navigation

The Help Center will feature a navigation menu with links to pages imagined to exist on an external website such as:

* Home
* About
* Pricing
* Testimonials
* Support
* Blog
* Legal

### Search Bar

The search bar appears to be an extremely central part of each Help Center, taking up the prime focus at the top center of each page. The following will be stated above the search bar:

> How can we help?
> 
> Use the search bar below to find the answer you are seeking. If none of our guides help you to resolve your specific issue, our support staff will be standing by to address your unique needs.

### Help Center Categories

Below the search bar, the most popular and thus useful articles should be featured, with the following categories and article titles. Each article will contain misc photos and 'Lorem Ipsum' content.

* General
    * Domain Registration and Setup
    * Setting up email accounts
    * Creating forwarding email addresses
    * Downloading Backups
* Advanced
    * Adding CNAME Record
    * Transfering accounts
* Resellers
    * Registering custom nameserver addresses

### Inspiration

For the purpose of a Hosting company, I've chosen the following existing Help Centers as the inspiration (and perhaps HTML/CSS source) for the custom branding applied to this demonstration Help Center.

* [Old Cloudflare](https://s-media-cache-ak0.pinimg.com/736x/c7/69/4b/c7694bb0aa6fd4fde0c7ff7c40f69d4b.jpg)
* [Cloudflare](https://support.cloudflare.com/hc/en-us)
* [xMatters Help Center](https://support.xmatters.com/hc/en-us)
* [Coursera Help Center](https://learner.coursera.help/hc/en-us?var=4&help=1)

### Implementation

I like the look and feel of the [Carousel example](http://getbootstrap.com/examples/carousel/) provided by Twitter Bootstrap. I'm going to replace the rotating carousel with the actual search bar, in a nice friendly large form.

Below this I'll arrange the articles by category using the grid system with 3 or 4 columns.


