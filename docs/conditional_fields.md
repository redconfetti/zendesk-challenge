## Conditional Fields for the Help Center

Taken from [Zendesk Apps Marketplace - Conditional Fields](https://www.zendesk.com/apps/conditional-fields/)

> The Conditional Fields app is an Enterprise-only app that lets you hide and show fields in your tickets to give agents and end-users a better user experience.

* [Introduction to Zendesk Apps](https://developer.zendesk.com/apps/docs/agent/introduction)
* [Zendesk App Tools](https://developer.zendesk.com/apps/docs/agent/tools)

### Notes

The [Conditional Fields app](https://www.zendesk.com/apps/conditional-fields/) is used only with Enterprise level accounts, or Professional accounts that are using the [Enterprise Productivity Pack add-on](https://www.zendesk.com/product/add-ons/). The app relies on two API endpoints:

1. [List Ticket Forms](https://developer.zendesk.com/rest_api/docs/core/ticket_forms#list-ticket-forms)
2. [List Ticket Fields](https://developer.zendesk.com/rest_api/docs/core/ticket_fields#list-ticket-fields)

Taken from [Creating ticket forms to support multiple request types (Professional Add-on and Enterprise)](https://support.zendesk.com/hc/en-us/articles/203661616-Creating-ticket-forms-to-support-multiple-request-types-Professional-Add-on-and-Enterprise-):

> You can use ticket forms to create multiple support request forms. A ticket form is a set of predefined ticket fields for a specific support request. The ticket form determines the fields and data a ticket contains.

Taken from [Using the Conditional Fields app (Professional Add-on and Enterprise)](https://support.zendesk.com/hc/en-us/articles/203662476-Using-the-Conditional-Fields-app-Enterprise-Only-):

> The Conditional Fields app is an Enterprise-only app that lets you hide and show fields in your tickets to give agents and end-users a better user experience. For example, if your ticket has a custom field that asks users to select an operating system, you can have two additional, hidden conditional fields -- one listing the latest Mac OS X versions and the other listing the latest Windows versions. If the user selects Windows as their OS, the field listing the Windows versions is shown. The field listing the OS X versions remains hidden.
>
> You can use conditional fields with regular tickets or with ticket forms. Ticket forms let you build different forms for different types of requests. For more information, see Creating ticket forms to support multiple request types (Enterprise).

### Help Center Application

Since this help center is being provided for a fictional hosting company, we will use the conditional fields app to present options to the customer to collect important information based on the type of inquiry, and if applicable the type of hosting they already have with the company.

* Issue Type (issue_type)
    * Sales (sales)
    * Customer Service (customer_service)
        * Customer Service Issue Type (customer_service_issue_type)
            * General Questions (general_questions)
            * Account Access (account_access)
            * Plan Changes (plan_changes)
    * Technical Support (technical_support)
        * Technical Support Issue Type (technical_support_issue_type)
            * Shared Hosting (shared_hosting)
                * Primary Domain Name
                * Issue Type
                    * Website
                    * Email
                    * DNS / Domain
            * Reseller Hosting (reseller_hosting)
                * Primary Domain Name (primary_domain_name)
                * Technical Issue Type (technical_issue_type)
                    * Website (website_issue)
                    * Email (email_issue)
                    * DNS / Domain (dns_issue)
            * Dedicated Hosting (dedicated_hosting)
                * IP Address (ip_address)
                * Root Authentication Type (root_auth_type)
                    * Root Password (root_password_auth)
                        * Root Password (root_password)
                    * SSH Key Only (ssh_key_auth)
                        * SSH Key (ssh_key)
                * Operating System (operating_system)
                    * Windows Server 2008 (windows_server_2008)
                    * Windows Server 2012 (windows_server_2012)
                    * CentOS (centos)
                    * Fedora (fedora)
                    * Ubuntu (ubuntu)
                    * Red Hat Enterprise (rhel)
                * Admin System (admin_system)
                    * cPanel (cpanel)
                    * Plesk (plesk)
                    * Webmin (webmin)
                    * Other (other)
