/*
 * jQuery v1.9.1 included
 */

$(document).ready(function() {

  $('#apodDate').datepicker();

  var getApod = function(date) {
    return $.ajax({
      url:      '/proxy/direct?url=https://api.nasa.gov/planetary/apod?api_key=8tKXFJvk4bzxmNizdRyj62p8ouqTEIo4LCoJO7FP&date=' + date,
      type:     'GET',
      dataType: 'json'
    });  
  };

  // load todays astronomy picture of the day
  var currentdate = new Date();
  var todaysDate = currentdate.getFullYear() + '-' + ('0' + (currentdate.getMonth()+1)).slice(-2) + '-' + ('0' + (currentdate.getDate())).slice(-2);

  var updateApod = function(title, url, explanation, date) {
    $('#apodError').html('');
    $('#apodErrorContainer').hide();
    $('#apodImage').attr('src', url);
    $('#apodDate').attr('value', date);
    $('#apodContent').show();
    $('#apodModalLabel').html(title);
    $('#apodModalImage').attr('src', url);
    $('#apodModalDescription').html(explanation);
  };

  var showError = function(error) {
    $('#apodContent').hide();
    $('#apodError').html(error);
    $('#apodErrorContainer').show();
  };

  var showLoading = function() {
    $('#apodErrorContainer').hide();
    $('#apodContent').hide();
    $('#apodLoader').show();
  };

  var showContent = function() {
    $('#apodErrorContainer').hide();
    $('#apodContent').show();
    $('#apodLoader').hide();
  };

  
  getApod(todaysDate).done(function(data) {
    updateApod(data.title, data.url, data.explanation, todaysDate);
  });

  // load onChange event for date selector
  $("#apodDate").change(function(e) {
    showLoading();
    e.preventDefault();
    var newDate = e.currentTarget.value;
    getApod(newDate).done(function(data){
      updateApod(data.title, data.url, data.explanation, newDate);
      showContent();
    }).fail(function(e) {
      if (e && e.responseJSON && e.responseJSON.msg) {
        showError(e.responseJSON.msg);
      } else {
        showError('You must choose a date between Jun 16, 1995 and today.');
      }
    });
  });
  
  // social share popups
  $(".share a").click(function(e) {
    e.preventDefault();
    window.open(this.href, "", "height = 500, width = 500");
  });

  // toggle the share dropdown in communities
  $(".share-label").on("click", function(e) {
    e.stopPropagation();
    var isSelected = this.getAttribute("aria-selected") == "true";
    this.setAttribute("aria-selected", !isSelected);
    $(".share-label").not(this).attr("aria-selected", "false");
  });

  $(document).on("click", function() {
    $(".share-label").attr("aria-selected", "false");
  });

  // show form controls when the textarea receives focus or backbutton is used and value exists
  var $answerbodyTextarea = $(".answer-body textarea"),
  $answerFormControls = $(".answer-form-controls"),
  $commentContainerTextarea = $(".comment-container textarea"),
  $commentContainerFormControls = $(".comment-form-controls");

  $answerbodyTextarea.one("focus", function() {
    $answerFormControls.show();
  });

  $commentContainerTextarea.one("focus", function() {
    $commentContainerFormControls.show();
  });

  if ($commentContainerTextarea.val() !== "") {
    $commentContainerFormControls.show();
  }

  if ($answerbodyTextarea.val() !== "") {
    $answerFormControls.show();
  }

  // Submit requests filter form in the request list page
  $("#request-status-select, #request-organization-select")
    .on("change", function() {
      search();
    });

  // Submit requests filter form in the request list page
  $("#quick-search").on("keypress", function(e) {
    if (e.which === 13) {
      search();
    }
  });

  function search() {
    window.location.search = $.param({
      query: $("#quick-search").val(),
      status: $("#request-status-select").val(),
      organization_id: $("#request-organization-select").val()
    });
  }

  // Submit organization form in the request page
  $("#request-organization select").on("change", function() {
    this.form.submit();
  });

});
